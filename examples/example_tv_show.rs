extern crate tmdb;

use tmdb::themoviedb::*;

fn main() {
    let tmdb = TMDb {
        api_key: env!("TMDB_API_KEY"),
        language: "en",
    };

    let page = tmdb
        .search()
        .title("The Office")
        .execute_tv()
        .unwrap();

    // let id = page.results[0].id;
    let shows = page.results;
    let show = shows[0].fetch(&tmdb).unwrap();
    let season = show.fetch_season(&tmdb, 1).unwrap();
    let episode = &season.episodes[0];
    println!("Episodes: {:#?}", &season.episodes);

}
